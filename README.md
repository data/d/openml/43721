# OpenML dataset: Hotel-Reviews

https://www.openml.org/d/43721

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data Description
Dataset is from one of the leading travel site containing hotel reviews provided by customers.  



Variable
Description




User_ID
unique ID of the customer


Description
description of the review posted


Browser_Used
browser used to post the review


Device_Used
device used to post the review


Is_Response
target Variable

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43721) of an [OpenML dataset](https://www.openml.org/d/43721). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43721/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43721/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43721/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

